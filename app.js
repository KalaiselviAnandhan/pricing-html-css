
function defaultToggle(){
    let button = document.getElementById("toggle-btn");
    button.classList.toggle("active");
}

let togglePeriods = true;
function addPrices(button, cname){
    button.toggle(cname);
    if(!togglePeriods){
        document.getElementById("basic-Price").innerHTML = 19.99;
        document.getElementById("professional-Price").innerHTML = 24.99;
        document.getElementById("master-Price").innerHTML = 39.99;
        togglePeriods = true;
    }
    else{
        document.getElementById("basic-Price").innerHTML = 199.99;
        document.getElementById("professional-Price").innerHTML = 249.99;
        document.getElementById("master-Price").innerHTML = 399.99;
        togglePeriods = false;
    }
}